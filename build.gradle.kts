// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    val hiltPluginVersion by extra("2.38.1")
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:7.1.1")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
        classpath("com.google.dagger:hilt-android-gradle-plugin:$hiltPluginVersion")
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }
}
