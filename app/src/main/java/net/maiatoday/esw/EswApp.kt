package net.maiatoday.esw

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EswApp : Application()