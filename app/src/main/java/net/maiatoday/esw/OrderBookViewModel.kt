package net.maiatoday.esw

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.maiatoday.esw.model.Order
import net.maiatoday.esw.model.OrderBook
import net.maiatoday.esw.model.OrderStatus
import net.maiatoday.esw.model.Tweet
import javax.inject.Inject
import kotlin.time.ExperimentalTime

@ExperimentalTime
@HiltViewModel
class OrderBookViewModel @Inject constructor() : ViewModel() {

    // This viewModel can fetch data from a repository but at the moment the data is completely fake
    var orderBook = mutableStateOf(OrderBook())
        private set

    private val possibleTweets: List<Tweet> = listOf(
        Tweet(),
        Tweet(tweet = "Arhem ok, cough cough"),
        Tweet(tweet = "La lalal alalaalla"),
        Tweet(tweet = "ok")
    )

    private var currentTweetIndex = 0

    var tweet = mutableStateOf(possibleTweets[currentTweetIndex])
        private set

    var loading = mutableStateOf(false)
        private set

    var order = mutableStateOf<Order?>(null)
        private set


    fun refreshTweet() {
        viewModelScope.launch {
            loading.value = true
            delay(3000)
            currentTweetIndex++
            if (currentTweetIndex == possibleTweets.size) {
                currentTweetIndex = 0
            }
            tweet.value = possibleTweets[currentTweetIndex]
            loading.value = false
        }
    }

    fun requestOrder(order: Order) {
        // roll the dice, 1 in 4 chance of a failed order
        //val orderSuccessPredictor = listOf(true, true, true, false).random()
        val orderSuccessPredictor = if (order.volume == 42.0f) false else true
        viewModelScope.launch {
            processOrder(order, orderSuccessPredictor)
        }
    }

    private var processingOrder = false
    internal val interval = 1000L

    /**
     * This is a fake order process, it wll be pending, then complete in multiples steps.
     * It will either fail or succeed depending on passed @orderSuccessPredictor
     */
    internal suspend fun processOrder(newOrder: Order, orderSuccessPredictor: Boolean = true) {
        this.order.value = newOrder
        // we know there is an order set but we need to test for null as the state starts at null
        if (!processingOrder && order.value != null) {
            processingOrder = true
            var filledAmount = 0.0f
            val fillIncrement: Float = (order.value?.volume ?: 0f) / 10
            while (processingOrder) {
                order.value?.let { currentOrder ->
                    processingOrder = when (currentOrder.status) {
                        OrderStatus.Pending -> {
                            if (orderSuccessPredictor) {
                                filledAmount += fillIncrement
                                if (filledAmount >= currentOrder.volume) {
                                    order.value = currentOrder.copy(
                                        status = OrderStatus.Success,
                                        volumeTraded = currentOrder.volume
                                    )
                                } else {
                                    order.value = currentOrder.copy(volumeTraded = filledAmount)
                                }
                            } else {
                                order.value = currentOrder.copy(status = OrderStatus.Fail)
                            }
                            true
                        }
                        OrderStatus.Success -> false
                        OrderStatus.Fail -> false
                    }
                    delay(interval)
                }
            }

        }
    }
}