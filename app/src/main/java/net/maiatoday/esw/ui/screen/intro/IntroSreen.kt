package net.maiatoday.esw.ui.screen

import android.content.res.Configuration
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import net.maiatoday.esw.R
import net.maiatoday.esw.ui.component.DogeCoin
import net.maiatoday.esw.ui.theme.EswTheme

const val SplashTimeout_ms = 8000L
@Composable
fun IntroScreen(onNextClick: () -> Unit) {
    EswTheme {

        // this fixes the bug where sometimes the timeout didn't make the intro go away
        val currentOnNextClick by rememberUpdatedState(onNextClick)

        LaunchedEffect(onNextClick) {
            delay(SplashTimeout_ms)
            currentOnNextClick()
        }
        Surface {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .clickable(onClick = onNextClick),
            ) {
                Image(
                    ImageVector.vectorResource(
                        id = R.drawable.ebon
                    ),
                    stringResource(R.string.desc_ebon),
                    colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                    modifier = Modifier
                        .align(Alignment.BottomStart)
                        .fillMaxWidth()
                )
                Image(
                    painterResource(R.mipmap.doge),
                    stringResource(R.string.desc_doge),
                    modifier = Modifier
                        .align(Alignment.BottomStart)
                        .size(256.dp),
                )
                Column(
                    modifier = Modifier
                        .padding(16.dp)
                        .align(Alignment.TopCenter)
                        .fillMaxHeight()
                ) {
                    Text(
                        "Ebon said",
                        style = MaterialTheme.typography.h3,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                    val infiniteTransition = rememberInfiniteTransition()
                    val whatSize by infiniteTransition.animateFloat(
                        initialValue = 0.5f,
                        targetValue = 1.5f,
                        animationSpec = infiniteRepeatable(
                            animation = tween(1000, easing = LinearEasing),
                            repeatMode = RepeatMode.Reverse
                        )
                    )
                    val whatColor by infiniteTransition.animateColor(
                        initialValue = MaterialTheme.colors.secondary,
                        targetValue = MaterialTheme.colors.onSurface,
                        animationSpec = infiniteRepeatable(
                            animation = tween(1000, easing = LinearEasing),
                            repeatMode = RepeatMode.Reverse
                        )
                    )

                    Text(
                        text = "Whah‽",
                        style = MaterialTheme.typography.h1,
                        color = whatColor,
                        modifier = Modifier.align(Alignment.CenterHorizontally).scale(whatSize)
                    )
                    CoinRow()
                }
            }
        }
    }
}

@Composable
fun CoinRow() {
    Row(modifier = Modifier
        .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        DogeCoin()
        DogeCoin()
        DogeCoin()
    }
}

@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL
)
@Composable
private fun DefaultPreview() {
    EswTheme {
        IntroScreen({})
    }
}