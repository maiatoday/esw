package net.maiatoday.esw.ui.screen

import android.content.res.Configuration
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import net.maiatoday.esw.R
import net.maiatoday.esw.model.CoinPair
import net.maiatoday.esw.model.OrderBook
import net.maiatoday.esw.model.OrderEntry
import net.maiatoday.esw.model.Tweet
import net.maiatoday.esw.ui.component.ButtonBuy
import net.maiatoday.esw.ui.component.ButtonSell
import net.maiatoday.esw.ui.component.LoadingOrTweetCard
import net.maiatoday.esw.ui.theme.EswTheme

@ExperimentalAnimationApi
@ExperimentalFoundationApi
@Composable
fun OrderBookScreen(
    tweet: Tweet,
    orderBook: OrderBook,
    loading: Boolean,
    onNextTweet: () -> Unit,
    onNextClick: (isBuy: Boolean) -> Unit
) {
    EswTheme {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text("${orderBook.pair.display} ${orderBook.priceMid}") },
                    backgroundColor = MaterialTheme.colors.primary
                )
            },
            content = {
                Column(
                    modifier = Modifier.fillMaxSize()
                ) {
                    LoadingOrTweetCard(
                        modifier = Modifier
                            .padding(8.dp)
                            .fillMaxWidth(),
                        tweet,
                        loading,
                        onNextTweet
                    )
                    Spacer(Modifier.size(8.dp))
                    Spread(
                        modifier = Modifier.fillMaxWidth(),
                        orderBook.priceBuy, orderBook.priceSell,
                        orderBook.pair
                    )
                    OrderBookDetails(orderBook.pair, orderBook.asks, orderBook.bids, onNextClick)
                }
            },
        )
    }
}

@Composable
fun Spread(modifier: Modifier = Modifier, buyPrice: Float, sellPrice: Float, pair: CoinPair) {
    val spread = sellPrice - buyPrice
    Text(
        modifier = modifier
            .background(MaterialTheme.colors.onSurface.copy(alpha = 0.1f))
            .padding(8.dp),
        style = MaterialTheme.typography.caption,
        text = stringResource(R.string.spread, pair.counter.symbol, spread)
    )
}

@ExperimentalFoundationApi
@Composable
fun OrderBookDetails(
    pair: CoinPair, asks: List<OrderEntry>, bids: List<OrderEntry>,
    onNextClick: (isBuy: Boolean) -> Unit
) {
    Row(
        modifier = Modifier.fillMaxSize(),
        // horizontalArrangement = Arrangement.SpaceAround
    ) {
        // first column takes half of the width
        OrderColumn(
            modifier = Modifier
                .weight(1.0f)
                .padding(8.dp),
            pair = pair,
            orders = bids,
            isBid = true,
            onNextClick = onNextClick
        )
        // second column takes all of what is left
        OrderColumn(
            modifier = Modifier
                .weight(1.0f)
                .padding(8.dp),
            pair = pair,
            orders = asks,
            isBid = false,
            onNextClick = onNextClick
        )
    }
}

@ExperimentalFoundationApi
@Composable
fun OrderColumn(
    modifier: Modifier = Modifier,
    pair: CoinPair,
    orders: List<OrderEntry>,
    isBid: Boolean,
    onNextClick: (isBuy: Boolean) -> Unit
) {
    val title = if (isBid) stringResource(R.string.bids) else stringResource(R.string.asks)
    LazyColumn(
        modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        stickyHeader(title) {
            Surface {
                Column(Modifier.fillMaxWidth()) {
                    if (isBid) {
                        ButtonBuy(modifier = Modifier.fillMaxWidth()) {
                            onNextClick(true)
                        }
                    } else {
                        ButtonSell(modifier = Modifier.fillMaxWidth()) {
                            onNextClick(false)
                        }
                    }

                    Spacer(Modifier.size(8.dp))
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = title,
                        style = MaterialTheme.typography.caption
                    )
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = "Price     Amount",
                        style = MaterialTheme.typography.body1
                    )
                    Spacer(Modifier.size(8.dp))
                }
            }
        }
        items(orders.size) { index ->
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = "${orders[index].displayPrice(pair.counter.decimals)}  ${
                    orders[index].displayVolume(
                        pair.base.decimals
                    )
                }",
                style = MaterialTheme.typography.body1,
                fontFamily = FontFamily.Monospace
            )
        }
    }

}

@ExperimentalAnimationApi
@ExperimentalFoundationApi
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL
)
@Composable
private fun DefaultPreview() {
    EswTheme {
        OrderBookScreen(Tweet(), OrderBook(), false, {}, {})
    }
}