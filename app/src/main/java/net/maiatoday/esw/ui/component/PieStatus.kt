package net.maiatoday.esw.ui.component

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

data class PieData(
    val foreground: Color = Color.White,
    val strokeWidth: Dp = 4.dp,
    val percentage: Float
)

@Composable
fun PieStatus(
    modifier: Modifier = Modifier,
    pieData: PieData
) {
    val currentPercentage = animateFloatAsState(
        targetValue =  pieData.percentage,
        animationSpec = tween(1000)
    )
    Canvas(
        modifier = modifier
    ) {
        val canvasWidth = size.width
        val canvasHeight = size.height
        drawCircle(
            color = pieData.foreground,
            center = Offset(x = canvasWidth / 2, y = canvasHeight / 2),
            radius = canvasWidth / 2 - pieData.strokeWidth.toPx(),
            style = Stroke(width = pieData.strokeWidth.toPx())
        )
        val arcPadding = pieData.strokeWidth.toPx() * 2
        drawArc(
            color = pieData.foreground,
            startAngle = -90f,
            sweepAngle = currentPercentage.value * 360,
            useCenter = true,
            topLeft = Offset(arcPadding, arcPadding),
            size = Size(size.width - (arcPadding * 2f), size.height - (arcPadding * 2f))
        )
    }
}