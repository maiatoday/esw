package net.maiatoday.esw.ui.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import net.maiatoday.esw.R

@Composable
fun DogeCoin(size: Dp = 64.dp) {
    val dogeCoin = painterResource(R.mipmap.dogecoin)
    val dogeCoinMod = Modifier
        .size(size)
    Image(
        dogeCoin,
        stringResource(R.string.desc_dogecoin),
        modifier = dogeCoinMod
    )
}

@Preview
@Composable
private fun DefaultPreview() {
    DogeCoin()
}