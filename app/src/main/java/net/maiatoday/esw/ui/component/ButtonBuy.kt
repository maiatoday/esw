package net.maiatoday.esw.ui.component

import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import net.maiatoday.esw.R
import net.maiatoday.esw.ui.theme.colorBuy

@Composable
fun ButtonBuy(modifier: Modifier = Modifier, onClick: () -> Unit) {
    Button(
        modifier = modifier,
        onClick = onClick,
        colors = ButtonDefaults.textButtonColors(
            backgroundColor = MaterialTheme.colors.colorBuy,
            contentColor = MaterialTheme.colors.onPrimary
        )
    ) { Text(stringResource(R.string.buy)) }
}

@Preview
@Composable
private fun DefaultPreview() {
    ButtonBuy { }
}