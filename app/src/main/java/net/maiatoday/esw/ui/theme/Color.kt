package net.maiatoday.esw.ui.theme

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Charcoal200 = Color(0xFF5C5C5C)
val Charcoal500 = Color(0xFF737373)
val Charcoal700 = Color(0xFF333333)
val Lime200 = Color(0xFFA5D6A7)
val Lime300 = Color(0xFF81C784)
val Lime500 = Color(0xFF4CAF50)
val Lime700 = Color(0xFF4CAF50)
val Tangerine200 = Color(0xFFFFCC80)
val Tangerine300 = Color(0xFFFFB74D)
val Tangerine500 = Color(0xFFFF9800)
val Tangerine700 = Color(0xFFF57C00)
val Rasberry200 = Color(0xFFF48FB1)
val Rasberry300 = Color(0xFFF06292)
val Rasberry500 = Color(0xFFE91E63)
val Rasberry700 = Color(0xFFC2185B)

val Colors.colorSell: Color
    @Composable get() = if (isLight) Rasberry500 else Rasberry300

val Colors.colorBuy: Color
    @Composable get() = if (isLight)Lime500 else Lime300

val Colors.avatar: Color
    @Composable get() = if (isLight) Color.Black.copy(alpha = 0.1f) else Color.White.copy(alpha = 0.9f)

val Colors.onAvatar: Color
    @Composable get() = Color.Black

val RainbowRed = Color(0xFFDA034E)
val RainbowOrange = Color(0xFFFF9800)
val RainbowYellow = Color(0xFFFFEB3B)
val RainbowGreen = Color(0xFF4CAF50)
val RainbowBlue = Color(0xFF2196F3)
val RainbowIndigo = Color(0xFF3F51B5)
val RainbowViolet = Color(0xFF9C27B0)

val SkittlesRainbow = listOf(
    RainbowRed,
    RainbowOrange,
    RainbowYellow,
    RainbowGreen,
    RainbowBlue,
    RainbowIndigo,
    RainbowViolet
)