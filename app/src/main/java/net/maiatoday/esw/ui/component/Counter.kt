package net.maiatoday.esw.ui.component

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.with
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Row
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import kotlin.math.log10
import kotlin.math.pow
import kotlin.math.roundToInt

@ExperimentalAnimationApi
@Composable
fun CounterDecimal(
    modifier: Modifier = Modifier,
    style: TextStyle = LocalTextStyle.current,
    fontWeight: FontWeight? = null,
    total: Float,
    decimals: Int
) {
    val wholeNumber = total.toInt()
    val fraction: Int = ((total - wholeNumber) * 10.0.pow(decimals)).roundToInt()
    val width = if (wholeNumber <= 0) 1 else log10(wholeNumber.toDouble()).toInt() + 1
    Counter(
        modifier = modifier,
        style = style,
        fontWeight = fontWeight,
        count = wholeNumber,
        width = width,
        onClick = {},
    )
    Text(".", style = style, fontWeight = fontWeight)
    Counter(
        modifier = modifier,
        style = style,
        fontWeight = fontWeight,
        count = fraction,
        width = decimals,
        onClick = {},
    )
}

@ExperimentalAnimationApi
@Composable
fun Counter(
    modifier: Modifier = Modifier,
    style: TextStyle = LocalTextStyle.current,
    fontWeight: FontWeight? = null,
    onClick: () -> Unit,
    count: Int,
    width: Int
) {
    val displayWidth = maxOf(1, width)
    Row(
        modifier = Modifier.clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = null,
            onClick = onClick
        ),
    ) {
        for (n in displayWidth - 1 downTo 0) {
            CounterCell(
                modifier = modifier,
                style = style,
                count = (count.toDouble() / 10.0.pow(n) % 10).toInt(),
                width = 1,
                fontWeight = fontWeight
            )
        }
    }
}

@ExperimentalAnimationApi
@Composable
fun CounterCell(
    modifier: Modifier = Modifier,
    style: TextStyle = LocalTextStyle.current,
    fontWeight: FontWeight? = null,
    count: Int,
    width: Int
) {
    val numbersSlidingAnimation: AnimatedContentScope<Int>.() -> ContentTransform = {
        if (initialState > targetState) {
            slideInVertically(initialOffsetY = { it }) + fadeIn() with slideOutVertically(
                targetOffsetY = { -it }) + fadeOut()
        } else {
            slideInVertically(initialOffsetY = { -it }) + fadeIn() with slideOutVertically(
                targetOffsetY = { it }) + fadeOut()
        }
    }
    AnimatedContent(
        targetState = count,
        transitionSpec = numbersSlidingAnimation
    ) { number ->
        Text(
            modifier = modifier,
            text = number.toString().padStart(width, '0'),
            style = style,
            fontWeight = fontWeight
        )
    }
}