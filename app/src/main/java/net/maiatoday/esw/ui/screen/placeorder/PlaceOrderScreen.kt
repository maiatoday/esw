package net.maiatoday.esw.ui.screen

import android.content.res.Configuration
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Tab
import androidx.compose.material.TabPosition
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import net.maiatoday.esw.R
import net.maiatoday.esw.model.Order
import net.maiatoday.esw.model.OrderBook
import net.maiatoday.esw.ui.component.FancyAnimatedIndicator
import net.maiatoday.esw.ui.component.NumberTextFieldWithError
import net.maiatoday.esw.ui.theme.EswTheme
import net.maiatoday.esw.ui.theme.colorBuy
import net.maiatoday.esw.ui.theme.colorSell

@ExperimentalComposeUiApi
@Composable
fun PlaceOrderScreen(
    orderBook: OrderBook,
    isBuy: Boolean = true,
    onNextClick: (order: Order) -> Unit,
    onBack: () -> Unit
) {
    EswTheme {
        val priceValue = if (isBuy) orderBook.priceBuy else orderBook.priceSell
        var price by rememberSaveable { mutableStateOf(priceValue.toString()) }
        var isPriceError by rememberSaveable { mutableStateOf(false) }

        var volume by rememberSaveable { mutableStateOf("") }
        var isVolumeError by rememberSaveable { mutableStateOf(false) }

        var note by rememberSaveable { mutableStateOf("") }

        var orderIsBuy by remember { mutableStateOf(isBuy) }
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text(orderBook.pair.display) },
                    backgroundColor = MaterialTheme.colors.primary,
                    navigationIcon = {
                        IconButton(onClick = onBack) {
                            Icon(Icons.Filled.ArrowBack, "Back")
                        }
                    }
                )
            },
            content = {
                Column(modifier = Modifier.fillMaxSize()) {
                    var state by remember { mutableStateOf(if (isBuy) 0 else 1) }
                    val titles = listOf(stringResource(R.string.buy), stringResource(R.string.sell))
                    val focusManager = LocalFocusManager.current
                    val indicator = @Composable { tabPositions: List<TabPosition> ->
                        FancyAnimatedIndicator(
                            tabPositions = tabPositions,
                            selectedTabIndex = state
                        )
                    }
                    TabRow(
                        selectedTabIndex = state,
                        indicator = indicator
                    ) {
                        titles.forEachIndexed { index, title ->
                            Tab(
                                text = { Text(title) },
                                selected = state == index,
                                onClick = {
                                    state = index
                                    orderIsBuy = if (index == 0) true else false
                                }
                            )
                        }
                    }
                    Spacer(Modifier.size(8.dp))
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceAround
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(16.dp)
                                .clickable {
                                    isPriceError = false
                                    price = orderBook.priceBuy.toString()
                                },
                            text = "Bid ${orderBook.pair.counter.display(orderBook.priceBuy)}",
                            color = MaterialTheme.colors.colorBuy,
                            style = MaterialTheme.typography.h6
                        )
                        Text(
                            modifier = Modifier
                                .padding(16.dp)
                                .clickable {
                                    isPriceError = false
                                    price = orderBook.priceSell.toString()
                                },
                            text = "Ask ${orderBook.pair.counter.display(orderBook.priceSell)}",
                            color = MaterialTheme.colors.colorSell,
                            style = MaterialTheme.typography.h6
                        )
                    }
                    Spacer(Modifier.size(8.dp))
                    NumberTextFieldWithError(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp),
                        value = price,
                        isError = isPriceError,
                        keyboardActions = KeyboardActions {
                            isPriceError = validateNumber(price)
                            if (!isPriceError) {
                                focusManager.moveFocus(FocusDirection.Down)
                            }
                        },
                        onValueChange = {
                            price = it
                            isPriceError = false
                        },
                        label = stringResource(R.string.price, orderBook.pair.counter.symbol),
                        errorMessage = "You need a price"
                    )
                    Spacer(Modifier.size(8.dp))
                    NumberTextFieldWithError(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp),
                        value = volume,
                        isError = isVolumeError,
                        keyboardActions = KeyboardActions {
                            isVolumeError = validateNumber(volume)
                            if (!isVolumeError) {
                                focusManager.moveFocus(FocusDirection.Down)
                            }
                        },
                        onValueChange = {
                            volume = it
                            isVolumeError = false
                        },
                        label = stringResource(R.string.volume, orderBook.pair.base.symbol),
                        errorMessage = "You need a volume"
                    ) {
                        Button(
                            modifier = Modifier.padding(4.dp),
                            onClick = { volume = "10" },
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = MaterialTheme.colors.secondary,
                                contentColor = MaterialTheme.colors.onSecondary
                            )
                        ) {
                            Text(text = "MAX", style = MaterialTheme.typography.caption)
                        }
                    }
                    Spacer(Modifier.size(8.dp))
                    OutlinedTextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp),
                        value = note,
                        singleLine = true,
                        onValueChange = { note = it },
                        label = {
                            Text(
                                stringResource(R.string.note),
                                color = MaterialTheme.colors.secondary
                            )
                        },
                        colors = TextFieldDefaults.outlinedTextFieldColors(focusedBorderColor = MaterialTheme.colors.secondary),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                        keyboardActions = KeyboardActions {
                            isVolumeError = validateNumber(volume)
                            isPriceError = validateNumber(price)
                            if (!isVolumeError && !isPriceError) {
                                placeOrder(
                                    orderIsBuy,
                                    price.toFloat(),
                                    volume.toFloat(),
                                    note,
                                    onNextClick
                                )
                            }
                        }
                    )
                    Spacer(modifier = Modifier.height(32.dp))
                    val buttonColor: Color by animateColorAsState(
                        targetValue = if (orderIsBuy) {
                            MaterialTheme.colors.colorBuy
                        } else {
                            MaterialTheme.colors.colorSell
                        },
                        animationSpec = tween(durationMillis = 1000)
                    )
                    val buttonText = if (orderIsBuy) stringResource(
                        R.string.buy
                    ) else stringResource(R.string.sell)
                    Button(
                        modifier = Modifier
                            .padding(16.dp)
                            .fillMaxWidth(),
                        onClick = {
                            isVolumeError = validateNumber(volume)
                            isPriceError = validateNumber(price)
                            if (!isVolumeError && !isPriceError) {
                                placeOrder(
                                    orderIsBuy,
                                    price.toFloat(),
                                    volume.toFloat(),
                                    note,
                                    onNextClick
                                )
                            }
                        },
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = buttonColor,
                            contentColor = MaterialTheme.colors.onPrimary
                        )
                    ) {
                        Text(buttonText)
                    }
                }
            },
        )
    }
}

private fun validateNumber(input: String): Boolean =
    input.toFloatOrNull() == null

private fun placeOrder(
    isBuy: Boolean,
    price: Float,
    volume: Float,
    note: String,
    onNextClick: (Order) -> Unit
) {
    onNextClick(
        Order(
            isBuy = isBuy,
            note = note,
            price = price,
            volume = volume
        )
    )
}

@ExperimentalComposeUiApi
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL
)
@Composable
private fun DefaultPreview() {
    EswTheme {
        PlaceOrderScreen(OrderBook(), true, {}, {})
    }
}