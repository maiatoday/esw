package net.maiatoday.esw.ui.screen

import android.os.Bundle
import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import com.google.accompanist.navigation.animation.composable
import androidx.navigation.navArgument
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import net.maiatoday.esw.OrderBookViewModel
import kotlin.time.ExperimentalTime

@ExperimentalFoundationApi
@ExperimentalComposeUiApi
@ExperimentalTime
@ExperimentalAnimationApi
@Composable
fun Navigation() {
    val navController = rememberAnimatedNavController()
    val viewModel: OrderBookViewModel = hiltViewModel()
    AnimatedNavHost(
        navController, startDestination = Screen.Intro.route,
        enterTransition = {
            slideIntoContainer(AnimatedContentScope.SlideDirection.Start, animationSpec = tween(700))
        },
        exitTransition = {
            // slideOutOfContainer doesn't work at the moment
            //slideOutOfContainer(AnimatedContentScope.SlideDirection.Start, animationSpec = tween(700))
            slideOutHorizontally(targetOffsetX = { -1000 }, animationSpec = tween(700))
        }
    ) {
        composable(
            route = Screen.Intro.route
        ) {
            IntroScreen(onNextClick = {
                navController.navigate(Screen.OrderBook.route) {
                    popUpTo(route = Screen.Intro.route) { inclusive = true }
                }
            })
        }
        composable(
            route = Screen.OrderBook.route,
            enterTransition = {
                when (initialState.destination.route) {
                    Screen.PlaceOrder.route ->
                        // if coming from PlaceOrder it was a back so slide in from left
                        slideInHorizontally(initialOffsetX = { -1000 }, animationSpec = tween(700))
                    Screen.Complete.route ->
                        // if coming from Complete it was a back so slide in from left
                        slideInHorizontally(initialOffsetX = { -1000 }, animationSpec = tween(700))
                    else -> null
                }
            }
        ) {
            val orderBook by viewModel.orderBook
            val tweet by viewModel.tweet
            val loading by viewModel.loading
            OrderBookScreen(
                tweet = tweet,
                orderBook = orderBook,
                loading = loading,
                onNextTweet = { viewModel.refreshTweet() },
                onNextClick = { isBuy ->
                    navController.navigate(
                        Screen.PlaceOrder.createRoute(isBuy)
                    )
                }
            )
        }
        composable(
            route = Screen.PlaceOrder.route,
            arguments = Screen.PlaceOrder.arguments,
            exitTransition = {
                if (targetState.destination.route == Screen.OrderBook.route) {
                    slideOutHorizontally(targetOffsetX = { 1000 }, animationSpec = tween(700))
                } else {
                    null
                }
            }

        ) { backStackEntry ->
            val isBuy = Screen.PlaceOrder.getIsBuy(backStackEntry.arguments)
            val orderBook by viewModel.orderBook
            PlaceOrderScreen(
                orderBook = orderBook,
                isBuy = isBuy,
                onNextClick = { order ->
                    viewModel.requestOrder(order)
                    navController.navigate(Screen.Complete.route) {
                        popUpTo(route = Screen.OrderBook.route)
                    }
                },
                onBack = {
                    navController.navigate(Screen.OrderBook.route) {
                        popUpTo(route = Screen.OrderBook.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(
            route = Screen.Complete.route,
            exitTransition = {
                if (targetState.destination.route == Screen.OrderBook.route) {
                    slideOutHorizontally(targetOffsetX = { 1000 }, animationSpec = tween(700))
                } else {
                    null
                }
            }
        ) {
            val order by viewModel.order
            CompleteScreen(
                order,
                onNextClick = {
                    navController.navigate(Screen.OrderBook.route) {
                        popUpTo(route = Screen.OrderBook.route) {
                            inclusive = true
                        }
                    }
                })
        }
    }
}

sealed class Screen(
    val route: String,
    val arguments: List<NamedNavArgument>
) {
    object Intro : Screen("intro", emptyList())
    object OrderBook : Screen("orderbook", emptyList())
    object PlaceOrder : Screen(
        route = "placeorder/{isBuy}",
        arguments = listOf(navArgument(isBuyArgName) { type = NavType.BoolType })
    ) {
        fun createRoute(isBuy: Boolean) = "placeorder/$isBuy"
        fun getIsBuy(arguments: Bundle?): Boolean = arguments?.getBoolean(isBuyArgName) ?: false
    }

    object Complete : Screen("complete", emptyList())
}

const val isBuyArgName = "isBuy"
