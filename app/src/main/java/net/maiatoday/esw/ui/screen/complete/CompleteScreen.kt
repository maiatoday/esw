package net.maiatoday.esw.ui.screen

import android.content.res.Configuration
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults.buttonColors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import net.maiatoday.esw.R
import net.maiatoday.esw.model.CoinPair
import net.maiatoday.esw.model.Order
import net.maiatoday.esw.model.OrderStatus
import net.maiatoday.esw.ui.component.*
import net.maiatoday.esw.ui.modifiers.ConfettiShape
import net.maiatoday.esw.ui.modifiers.confetti
import net.maiatoday.esw.ui.theme.EswTheme
import net.maiatoday.esw.ui.theme.SkittlesRainbow
import kotlin.time.ExperimentalTime

@ExperimentalAnimationApi
@ExperimentalTime
@Composable
fun CompleteScreen(order: Order?, onNextClick: () -> Unit) {
    EswTheme {
        CompleteFromOrder(order, onNextClick)
    }
}

@ExperimentalAnimationApi
@Composable
fun CompleteFromOrder(order: Order?, onNextClick: () -> Unit) {
    Surface {
        Column(
            Modifier
                .fillMaxSize()
                .confetti(
                    isVisible = (order?.status == OrderStatus.Success),
                    contentColors = SkittlesRainbow,
                    speed = 0.1f,
                    populationFactor = 0.35f,
                    confettiShape = ConfettiShape.Rectangle
                ),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (order != null) {
                StatusImage(order.status)
                Spacer(modifier = Modifier.height(32.dp))
                OrderDetail(order)
                OrderFillState(order.volumeTraded, order.volume, order.pair.base.decimals)
                Spacer(modifier = Modifier.height(32.dp))
                OrderTotalCost(order)
            } else {
                Text("Oops")
            }
            Spacer(modifier = Modifier.height(32.dp))
            Button(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                onClick = onNextClick,
                colors = buttonColors(
                    backgroundColor = MaterialTheme.colors.secondary,
                    contentColor = MaterialTheme.colors.onSecondary
                )
            ) {
                Text(stringResource(R.string.next))
            }

        }
    }
}


@ExperimentalAnimationApi
@Composable
fun OrderTotalCost(order: Order) {
    val total = order.price * order.volumeTraded
    val style = MaterialTheme.typography.h4
    Row {
        Text(text = "Total: ", style = style)
        Text(text = "${order.pair.counter.symbol} ", style = style, fontWeight = FontWeight.Bold)
        CounterDecimal(
            style = style,
            fontWeight = FontWeight.Bold,
            total = total,
            decimals = order.pair.counter.decimals
        )
    }
}

@ExperimentalAnimationApi
@Composable
fun OrderFillState(volumeTraded: Float, volume: Float, decimals: Int) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        val pieData = PieData(
            foreground = MaterialTheme.colors.secondary,
            percentage = volumeTraded / volume
        )
        PieStatus(
            Modifier
                .size(56.dp)
                .padding(4.dp), pieData
        )
        Row {
            Text("Traded: ")
            CounterDecimal(total = volumeTraded, decimals = decimals)
        }
    }
}

@Composable
fun OrderDetail(order: Order) {
    val action = when {
        (order.isBuy and (order.status == OrderStatus.Pending)) -> "Buying"
        (order.isBuy and (order.status == OrderStatus.Success)) -> "Bought"
        (!order.isBuy and (order.status == OrderStatus.Pending)) -> "Selling"
        (!order.isBuy and (order.status == OrderStatus.Success)) -> "Sold"
        (order.isBuy and (order.status == OrderStatus.Fail)) -> "Buy Failed"
        (!order.isBuy and (order.status == OrderStatus.Fail)) -> "Sell Failed"
        else -> "Unknown"
    }
    Row {
        Text(
            text = "$action "
        )
        Text(
            text = order.pair.base.display(order.volume),
            fontWeight = FontWeight.Bold
        )
    }
    Row {
        Text(text = "at ")
        Text(
            text = order.pair.counter.display(order.price),
            fontWeight = FontWeight.Bold
        )
        Text(
            text = "/${order.pair.base.symbol}"
        )
    }
}

@ExperimentalAnimationApi
@Composable
fun StatusImage(status: OrderStatus) {
    Image(
        painterResource(R.drawable.onlydoge),
        stringResource(R.string.desc_doge),
        modifier = Modifier
            .size(128.dp),
    )
    val statusMessage = when (status) {
        OrderStatus.Pending -> "In Progress"
        OrderStatus.Success -> "Yippeee!"
        OrderStatus.Fail -> "Oops"
    }
    if (status == OrderStatus.Success) {
        RainbowText(text = statusMessage, style = MaterialTheme.typography.h2)
    } else {
        Text(text = statusMessage, style = MaterialTheme.typography.h2)
    }
}

@ExperimentalAnimationApi
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL
)
//@Preview(
//    showBackground = true,
//    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL
//)
@Composable
private fun DefaultPreview() {
    EswTheme {
        CompleteFromOrder(
            Order(CoinPair(), 0.2f, 0.005f, "blah", true),
            {}
        )
    }
}