package net.maiatoday.esw.ui.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import net.maiatoday.esw.ui.modifiers.confetti

@Composable
fun Sprinkle() {
    Box(modifier = Modifier
        .background(Color.Black)
        .fillMaxSize()
        .confetti()

    ) {
        Text("Hello", color = Color.Red)
    }

}

@Preview
@Composable
private fun DefaultPreview() {
    Sprinkle()
}
