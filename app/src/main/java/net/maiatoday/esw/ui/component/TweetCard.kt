package net.maiatoday.esw.ui.component

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandIn
import androidx.compose.animation.shrinkOut
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import net.maiatoday.esw.R
import net.maiatoday.esw.model.Tweet
import net.maiatoday.esw.ui.modifiers.ConfettiShape
import net.maiatoday.esw.ui.modifiers.confetti
import net.maiatoday.esw.ui.theme.avatar
import net.maiatoday.esw.ui.theme.onAvatar

@ExperimentalAnimationApi
@Composable
fun LoadingOrTweetCardFancy(
    modifier: Modifier = Modifier,
    tweet: Tweet,
    loading: Boolean,
    onClick: () -> Unit
) {
    AnimatedVisibility(
        visible = loading,
        enter = expandIn(
            expandFrom = Alignment.CenterStart,
            animationSpec = tween(durationMillis = 1000)
        ),
        exit = shrinkOut(
            shrinkTowards = Alignment.CenterStart,
            animationSpec = tween(durationMillis = 1000)
        )
    ) {
        LoadingIndicator(modifier)
    }
    AnimatedVisibility(
        visible = !loading,
        enter = expandIn(
            expandFrom = Alignment.CenterStart,
            animationSpec = tween(durationMillis = 1000)
        ),
        exit = shrinkOut(
            shrinkTowards = Alignment.CenterStart,
            animationSpec = tween(durationMillis = 1000)
        )
    ) {
        TweetCard(modifier, tweet, onClick)
    }
}

@ExperimentalAnimationApi
@Composable
fun LoadingOrTweetCard(
    modifier: Modifier = Modifier,
    tweet: Tweet,
    loading: Boolean,
    onClick: () -> Unit
) {
    Crossfade(targetState = loading, animationSpec = tween(durationMillis = 2000)) { isLoading ->
        when (isLoading) {
            true -> LoadingIndicator(modifier = modifier)
            false -> TweetCard(modifier, tweet, onClick)
        }
    }

}

@Composable
private fun TweetCard(
    modifier: Modifier,
    tweet: Tweet,
    onClick: () -> Unit
) {
    Card(
        modifier = modifier.defaultMinSize(minHeight = 128.dp),
        border = BorderStroke(1.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.2f)),
        elevation = 4.dp
    ) {
        Row(
            Modifier
                .clip(RoundedCornerShape(4.dp))
                .background(MaterialTheme.colors.surface)
                .clickable(onClick = onClick)
                .padding(16.dp)
        ) {
            Surface(
                modifier = Modifier.size(56.dp),
                shape = CircleShape,
                color = MaterialTheme.colors.avatar
            ) {
                Image(
                    modifier = Modifier.padding(8.dp),
                    imageVector = ImageVector.vectorResource(
                        id = R.drawable.ebon_head
                    ),
                    contentDescription = tweet.name,
                    colorFilter = ColorFilter.tint(MaterialTheme.colors.onAvatar)
                )
            }
            Column(modifier = Modifier.padding(8.dp)) {
                Text(tweet.tweet, fontWeight = FontWeight.Bold)
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                    Text(tweet.username, style = MaterialTheme.typography.body2)
                }
            }
        }
    }
}

@Composable
private fun LoadingIndicator(modifier: Modifier) {
    Surface(modifier.defaultMinSize(minHeight = 128.dp)) {
        Box(
            modifier = modifier
                .defaultMinSize(minHeight = 128.dp)
                .confetti(
                    isVisible = true,
                    contentColors = listOf(
                        MaterialTheme.colors.secondary,
                        MaterialTheme.colors.secondaryVariant,
                        MaterialTheme.colors.primary,
                        MaterialTheme.colors.primaryVariant,
                        MaterialTheme.colors.onSurface,
                    ),
                    speed = 0.2f,
                    populationFactor = 2.0f,
                    confettiShape = ConfettiShape.Circle
                )
        ) {
            CircularProgressIndicator(
                Modifier
                    .requiredSize(24.dp)
                    .align(Alignment.Center),
                color = MaterialTheme.colors.secondary
            )

        }
    }
}


@ExperimentalAnimationApi
@Preview
@Composable
private fun DefaultPreview() {
    var loading by remember { mutableStateOf(false) }
    LoadingOrTweetCard(tweet = Tweet(), loading = loading, onClick = { loading = !loading })
}