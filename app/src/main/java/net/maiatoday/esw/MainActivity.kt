package net.maiatoday.esw

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import dagger.hilt.android.AndroidEntryPoint
import net.maiatoday.esw.ui.screen.Navigation
import net.maiatoday.esw.ui.theme.EswTheme
import kotlin.time.ExperimentalTime

@ExperimentalAnimationApi
@ExperimentalTime
@ExperimentalFoundationApi
@ExperimentalComposeUiApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EswTheme {
                EswRoot()
            }
        }
    }
}


@ExperimentalFoundationApi
@ExperimentalComposeUiApi
@ExperimentalTime
@ExperimentalAnimationApi
@Composable
fun EswRoot() {
    Navigation()
}
