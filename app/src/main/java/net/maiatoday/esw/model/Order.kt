package net.maiatoday.esw.model

data class Order(
    val pair: CoinPair = CoinPair(),
    val price: Float,
    val volume: Float,
    val note: String,
    val isBuy: Boolean,
    var status: OrderStatus = OrderStatus.Pending,
    var volumeTraded: Float = 0.0f
)

enum class OrderStatus {
    Pending,
    Success,
    Fail
}