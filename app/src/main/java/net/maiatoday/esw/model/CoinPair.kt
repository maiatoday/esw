package net.maiatoday.esw.model

data class CoinPair(
    val base: Coin = Coin(
        name = "DogeCoin",
        symbol = "DOGE",
        decimals = 6,
        isCrypto = true
    ),
    val counter: Coin = Coin(
        name = "US Dollar",
        symbol = "USD",
        decimals = 2,
        isCrypto = false
    )
) {
    val display: String
        get() = "${base.symbol}/${counter.symbol}"
}