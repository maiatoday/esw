package net.maiatoday.esw.model

import kotlin.random.Random

data class OrderBook(
    val pair: CoinPair = CoinPair(),
    val bids: List<OrderEntry> = generateFakeOrders(40, 0.01F, 0.19F, true),
    val asks: List<OrderEntry> = generateFakeOrders(40, 0.21F, 1.00F, false)
) {
    val priceMid: Float
        get() = (priceBuy + priceSell) / 2
    val priceBuy: Float
        get() = bids[0].price
    val priceSell: Float
        get() = asks[0].price
}

private fun generateFakeOrders(
    count: Int = 10,
    lowPrice: Float = 0.19F,
    highPrice: Float = 0.21F,
    descendingPrice: Boolean = true
): List<OrderEntry> {
    val orders: MutableList<OrderEntry> = mutableListOf()
    if ((count <= 0) or (highPrice <= lowPrice)) return orders
    val increment = if (descendingPrice) {
        (lowPrice - highPrice) / count
    } else {
        (highPrice - lowPrice) / count
    }
    var price = if (descendingPrice) highPrice else lowPrice
    for (i in 0..count) {
        orders.add(OrderEntry(price, Random.nextFloat() * 5))
        price += increment
    }
    return orders
}