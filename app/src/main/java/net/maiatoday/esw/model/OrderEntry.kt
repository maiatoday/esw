package net.maiatoday.esw.model

data class OrderEntry(val price: Float, val volume: Float) {
    fun displayPrice(decimals:Int = 2) = "%.${decimals}f".format(price)
    fun displayVolume(decimals:Int = 6) = "%.${decimals}f".format(volume)
}