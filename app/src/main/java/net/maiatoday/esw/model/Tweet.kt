package net.maiatoday.esw.model

import net.maiatoday.esw.R

data class Tweet(
    val name: String = "Ebon Musk",
    val username: String = "@theRealEbon",
    val tweet: String = "Buy, buy buy! Sell, buy! Baby 🦈🦈🦈 do do dodo dedoodoo more ramblings for a longer tweet",
    val avatar: Int = R.drawable.ebon_head
)