package net.maiatoday.esw.model

data class Coin(
    val symbol: String,
    val name: String,
    val decimals: Int,
    val isCrypto: Boolean = true
) {
    fun display(value: Float) = "${symbol} %.${decimals}f".format(value)
}