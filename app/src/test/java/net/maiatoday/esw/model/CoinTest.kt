package net.maiatoday.esw.model

import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class CoinTest {
    @Test
    fun `should format coin display`() {
        val coin = Coin(
            name = "DogeCoin",
            symbol = "DOGE",
            decimals = 6,
            isCrypto = true
        )
        val tt = coin.display(0.12345678f)
        assertThat(coin.display(0.12345678f), equalTo("DOGE 0.123457"))
    }
}