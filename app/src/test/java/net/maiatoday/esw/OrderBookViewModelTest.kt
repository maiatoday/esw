package net.maiatoday.esw

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import net.maiatoday.esw.model.CoinPair
import net.maiatoday.esw.model.Order
import net.maiatoday.esw.model.OrderStatus
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class OrderBookViewModelTest {

    @Test
    fun `should fail if success predictor is false`() = runBlockingTest {
        // given the view model
        pauseDispatcher()
        val viewModel = OrderBookViewModel()
        // when an order process is started and the predictor is false
        val newOrder = Order(CoinPair(), 0.01f, 1.0f, "test", true)
        viewModel.processOrder(newOrder, false)
        runCurrent()
        // then the order should fail
        assertThat(viewModel.order.value?.status, equalTo(OrderStatus.Fail))
    }

    @Test
    fun `should succeed after some time if success predictor is true`() = runBlockingTest{
        // given the view model
        pauseDispatcher()
        val viewModel = OrderBookViewModel()
        // when an order process is started and the predictor is true
        val newOrder = Order(CoinPair(), 0.01f, 1.0f, "test", true)
        viewModel.processOrder(newOrder, true)
        advanceUntilIdle()
        // then the order should succeed
        assertThat(viewModel.order.value?.status, equalTo(OrderStatus.Success))
    }
}